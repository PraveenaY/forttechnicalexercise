Steps to run API:
Step 1: 
Clone the repository from git: git clone https://PraveenaY@bitbucket.org/PraveenaY/forttechnicalexercise.git
repository bitbucket link: https://bitbucket.org/PraveenaY/forttechnicalexercise/src

Step 2: 
After cloning, git will point to 'master' branch.

Step 3: Navigate to the solution folder

Step 4: Run/update docker compose: 'docker-compose up -d --build'

Step 5: Once docker compose has been deployed successfully, we can access the api at 'http://localhost:8100'.

Step 6: We can now use POSTMAN/FIDDLER apps to test the WebAPI. API calls are mentioned in API.md file
NOTE: The database will be DROPPED AND RECREATED for every applicaiton start.
___________________________________________________________________________________________________________________
