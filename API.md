Have used PostMan app to test the WebAPI.
Have used BasicAuthentication to validate email and password before accessing the API method.
So, when testing from POSTMAN/FIDDLER please enable BasicAuth to provide username and password for authentication.

API calls for Managing User - (Database Master Table - "Users")

1) Get All Users
HTTP Verb - HTTPGET
http://localhost:8100/api/Users

2) Get User By Id
HTTP Verb - HTTPGET
http://localhost:8100/api/Users/1

3) Add User  -- This is Allow Anonymous call, so, no Authentication needed
HTTP Verb - HTTPPOST
http://localhost:8100/api/Users
sample json input from Body:  {"Name":"PraveenaY", "Email" : "Praveenaybs@gmail.com", "Password":"Test@123"}

4) Update User  - Update existing details
HTTP Verb - HTTPPUT
http://localhost:8100/api/Users/1
sample json input from Body: {"Id": 1, "Name":"PraveenaYBS", "Email" : "Praveenaybs@gmail.com", "Password":"Test@123"}

5) Delete User - Delete existing user
HTTP Verb - HTTPDELETE
http://localhost:8100/api/Users/1


API calls for Managing User Detail - Favourite City and Country  - (Database Master Table - "Location")

1) Get All User Favourite City,Country by Id
HTTP Verb - HTTPGET
http://localhost:8100/api/Location/1

2) Add Location - City and Country
HTTP Verb - HTTPPOST
http://localhost:8100/api/Location
sample json input from Body:  {"UserId":1, "CityName" : "Mumbai", "Country":"India"}
