﻿using FortCode.Data;
using FortCode.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FortCode.UnitTests
{
    public class FortCodeTestHelper
    {
        private readonly UserDbContext userDbContext;
        public FortCodeTestHelper()
        {
            var builder = new DbContextOptionsBuilder<UserDbContext>();
            builder.UseInMemoryDatabase(databaseName: "FortCodeDBInMemory");

            var dbContextOptions = builder.Options;
            userDbContext = new UserDbContext(dbContextOptions);
            // Delete existing db before creating a new one
            userDbContext.Database.EnsureDeleted();
            userDbContext.Database.EnsureCreated();
        }

        public IUserRepository GetUserRepository()
        {
            return new UserRepository(userDbContext);
        }

        public ILocationRepository GetLocationRepository()
        {
            return new LocationRepository(userDbContext);
        }

    }
}
