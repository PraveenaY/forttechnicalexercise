using FortCode.Exceptions;
using FortCode.Models;
using FortCode.Models.DTO;
using System;
using System.Linq;
using Xunit;

namespace FortCode.UnitTests
{
    public class UserTests
    {
        [Fact]
        public void AddUser_ShouldAddUser()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();
            // adding mock data
            userRepository.AddUser(new UserViewModel()
            {
                Name = "Test1",
                Email = "test@me.com",
                Password = "Test@123"
            });


            //Act
            // Pass UserId as 1 to fetch the just added User
            var result = userRepository.GetUser(1).Result;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Test1", result.Name);
            Assert.Equal("test@me.com", result.Email);
        }

        [Fact]
        public void AddUser_ShouldThrowException_WhenEmailIsMissing()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();

            var user = new UserViewModel()
            {
                Name = "Test1",
                Password = "Test@123"
            };

            //Act
            var exception = Record.ExceptionAsync(() => userRepository.AddUser(user));

            // Assert
            Assert.IsType<ArgumentNullException>(exception.Result);
        }

        [Fact]
        public void AddUser_ShouldThrowException_WhenEmailAlreadyExists()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();
            // adding mock data
            userRepository.AddUser(new UserViewModel()
            {
                Name = "Test1",
                Email = "test@me.com",
                Password = "Test@123"
            });

            //model to add duplicate data
            var user = new UserViewModel()
            {
                Name = "Test1",
                Email = "test@me.com",
                Password = "Test@123"
            };

            //Act
            var exception = Record.ExceptionAsync(() => userRepository.AddUser(user));

            // Assert
            Assert.IsType<AppException>(exception.Result);
            Assert.Equal("Email already exists.", exception.Result.Message);
        }

        [Fact]
        public void GetUser_ShouldReturnUser_WhenUserExist()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();
            // adding mock data
            var userMockData = userRepository.AddUser(new UserViewModel()
            {
                Name = "Test1",
                Email = "test@me.com",
                Password = "Test@123"
            });


            //Act
            // Pass UserId as 1 to fetch the just added User
            var actualUserData = userRepository.GetUser(1).Result;

            // Assert
            Assert.NotNull(userMockData.Result);
            Assert.Equal("Test1", userMockData.Result.Name);
            Assert.Equal("test@me.com", userMockData.Result.Email);
        }

        [Fact]
        public void GetUser_ShouldReturnNull_WhenUserDoesNotExist()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();
            // adding mock data


            //Act
            // Pass UserId as 2 to fetch the just added User
            var actualUserData = userRepository.GetUser(1).Result;

            // Assert
            Assert.Null(actualUserData);
        }

        [Fact]
        public void GetUsers_ShouldReturnAllUsers_WhenExist()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();

            // adding mock data
            userRepository.AddUser(new UserViewModel()
            {
                Name = "Test1",
                Email = "test@me.com",
                Password = "Test@123"
            });
            userRepository.AddUser(new UserViewModel()
            {
                Name = "Test2",
                Email = "test2@me2.com",
                Password = "Test@321"
            });

            var mockInsertCount = 2;

            //Act
            var actualUserData = userRepository.GetUsers().Result;

            // Assert
            Assert.Equal(mockInsertCount, actualUserData.Count());
        }

        [Fact]
        public void GetUsers_ShouldReturnNull_WhenNoUsersExist()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();
            var mockInsertCount = 0;


            //Act
            var actualUserData = userRepository.GetUsers().Result;

            // Assert
            Assert.Equal(mockInsertCount, actualUserData.Count());
        }

        [Fact]
        public void UpdateUser_ShouldUpdateUserDetail_WhenUserExists()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();
            // adding mock data
            userRepository.AddUser(new UserViewModel()
            {
                Name = "Test1",
                Email = "test@me.com",
                Password = "Test@123"
            });

            userRepository.UpdateUser(1, new UserViewModel()
            {
                Name = "Test2",
                Email = "test2@me2.com",
                Password = "Test@222"
            });

            //Act
            // Pass UserId as 1 to fetch the just added User
            var result = userRepository.GetUser(1).Result;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Test2", result.Name);
            Assert.Equal("test2@me2.com", result.Email);
        }

        [Fact]
        public void DeleteUser_ShouldRemoveUserAndLocationDetail_WhenUserExists()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();
            // adding mock data
            userRepository.AddUser(new UserViewModel()
            {
                Name = "Test2",
                Email = "test2@me2.com",
                Password = "Test@222"
            });

            //Act
            // Pass UserId as 1 to fetch the just added User
            var isUserRemoved = userRepository.DeleteUser(1);
            // Pass UserId as 1 to fetch User
            var userAfterDelete = userRepository.GetUser(1).Result;

            // Assert
            Assert.Null(userAfterDelete);
            Assert.True(isUserRemoved.Result);
        }

        [Fact]
        public void GetLocationDetail_ShouldReturnLocationDetail_WhenUserExists()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();
            var locationRepository = testHelper.GetLocationRepository();

            // adding mock data
            userRepository.AddUser(new UserViewModel()
            {
                Name = "Test1",
                Email = "test@me.com",
                Password = "Test@123"
            });

            locationRepository.AddLocationDetail(1, new LocationViewModel()
            {
                CityName = "Mumbai",
                Country = "India"
            });

            locationRepository.AddLocationDetail(1, new LocationViewModel()
            {
                CityName = "Hyderabad",
                Country = "India"
            });


            //Act
            var actualUserData = locationRepository.GetLocationDetail(1).Result;

            // Assert
            Assert.NotNull(actualUserData);
            Assert.Equal(2, actualUserData.Count());
        }

        [Fact]
        public void GetLocationDetail_ShouldReturnNoRecords_WhenUserDoesNotExists()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var locationRepository = testHelper.GetLocationRepository();

            // adding mock data

            //Act
            var actualUserData = locationRepository.GetLocationDetail(1).Result;

            // Assert
            Assert.True(actualUserData.Count() == 0);
        }

        [Fact]
        public void DeleteUser_ShouldDeleteLocationDetail_WhenUserExists()
        {
            var testHelper = new FortCodeTestHelper();

            //Arrange
            // InMemory Repositories
            var userRepository = testHelper.GetUserRepository();
            var locationRepository = testHelper.GetLocationRepository();

            // adding mock data
            userRepository.AddUser(new UserViewModel()
            {
                Name = "Test1",
                Email = "test@me.com",
                Password = "Test@123"
            });

            locationRepository.AddLocationDetail(1, new LocationViewModel()
            {
                CityName = "Mumbai",
                Country = "India"
            });

            locationRepository.AddLocationDetail(1, new LocationViewModel()
            {
                CityName = "Hyderabad",
                Country = "India"
            });

            userRepository.DeleteUser(1);

            //Act
            var user = userRepository.GetUser(1).Result;
            var locationDetail = locationRepository.GetLocationDetail(1).Result;

            // Assert
            Assert.Null(user);
            Assert.True(locationDetail.Count() == 0);
        }

    }
}
