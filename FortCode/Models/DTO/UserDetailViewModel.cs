﻿using FortCode.Models.Entities;
using System.Collections.Generic;

namespace FortCode.Models.DTO
{
    public class UserDetailViewModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public List<Location> LocationDetail { get; set; }
    }
}
