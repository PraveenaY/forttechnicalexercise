﻿namespace FortCode.Models.DTO
{
    public class LocationViewModel
    {
        public string CityName { get; set; }
        public string Country { get; set; }
    }
}
