﻿namespace FortCode.Models.DTO
{
    public class LocationDetailViewModel
    {
        public int UserId { get; set; }
        public string CityName { get; set; }

        public string Country { get; set; }

    }
}
