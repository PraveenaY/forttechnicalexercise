﻿namespace FortCode.Models.Entities
{
    public class Location
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public  User User { get; set; }
        public string CityName { get; set; }

        public string Country { get; set; }

    }
}
