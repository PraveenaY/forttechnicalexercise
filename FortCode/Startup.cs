using FortCode.AuthenticationHandler;
using FortCode.Data;
using FortCode.Exceptions;
using FortCode.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace FortCode
{
    public class Startup
    {
        private IConfiguration Config { get; }

        public Startup(IConfiguration config)
        {
            Config = config;
        }
        public virtual void ConfigureServices(IServiceCollection services)
        {
            string connectionString;
            connectionString = Environment.GetEnvironmentVariable("ConnectionStrings:DbContext");
            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = Config["ConnectionStrings:DbContext"];
            }
            services.AddDbContext<Data.UserDbContext>(
                options => options.UseSqlServer(connectionString));

            services
                .AddMvc();
            services
                .AddCors();

            services
                .AddControllers(); 

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();

            services.AddAuthentication("BasicAuthentication")
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            app
                .UseFileServer()
                .UseRouting();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<ExceptionHandler>();

            app.UseEndpoints(endPoints => { endPoints.MapControllers(); });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            var context = serviceScope.ServiceProvider.GetRequiredService<UserDbContext>();
            context.Database.EnsureDeleted();
            context.Database.Migrate();
        }
    }
}
