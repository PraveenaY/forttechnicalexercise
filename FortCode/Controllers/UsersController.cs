﻿using FortCode.Data;
using FortCode.Models;
using FortCode.Models.DTO;
using FortCode.Models.Entities;
using FortCode.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            var users = await _userRepository.GetUsers();
            return Ok(users);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDetailViewModel>> GetUser(int id)
        {
            var user = await _userRepository.GetUser(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, UserViewModel user)
        {
            if (ModelState.IsValid)
            {
                await _userRepository.UpdateUser(id, user);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }



        // POST: api/Users
        [HttpPost]
        public async Task<ActionResult<User>> AddUser(UserViewModel user)
        {
            if (ModelState.IsValid)
            {
                var userData = await _userRepository.AddUser(user);
                if (userData == null)
                {
                    return Conflict(new { message = $"Email already exists - '{user.Email}'." });
                }
                else
                {
                    return CreatedAtAction("GetUser", new { id = userData.Id }, user);
                }
            }
            else
            {
                return BadRequest();
            }
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<bool> DeleteUser(int id)
        {
            return await _userRepository.DeleteUser(id);
        }

    }
}
