﻿using FortCode.Models.DTO;
using FortCode.Models.Entities;
using FortCode.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
namespace FortCode.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LocationController : ControllerBase
    {
        private readonly ILocationRepository _locationRepository;
        private readonly int loggedInUserId;

        public LocationController(ILocationRepository locationRepository,
            IHttpContextAccessor httpContextAccessor)
        {
            _locationRepository = locationRepository;
            loggedInUserId = int.Parse(httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<LocationDetailViewModel>>> GetLocationDetail()
        {
            var detailList = await _locationRepository.GetLocationDetail(loggedInUserId);

            return Ok(detailList);
        }

        [HttpPost]
        public async Task<ActionResult<Location>> AddLocations([FromBody] LocationViewModel[] locations)
        {
            if (ModelState.IsValid)
            {
                var locationAdded = false;
                foreach (var location in locations)
                {
                    var isCityAndCountryExists = 
                        await _locationRepository.FindLocation(loggedInUserId, location.CityName, location.Country);
                    if (!isCityAndCountryExists)
                    {
                        await _locationRepository.AddLocationDetail(loggedInUserId, location);
                        locationAdded = true;
                    }
                }
                if (locationAdded)
                {
                    return Ok();
                }
                else
                {
                    return Conflict(new { message = $"Location details already exists." });
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete("{locationId}")]
        public async Task<ActionResult> DeleteUser(int locationId)
        {
            var result = await _locationRepository.DeleteLocation(loggedInUserId, locationId);
            if (result)
            {
                return Ok();
            }
            else
            {
                return Conflict(new { message = $"Failed to delete location." });
            }
        }
    }
}
