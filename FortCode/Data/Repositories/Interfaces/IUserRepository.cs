﻿using FortCode.Models.DTO;
using FortCode.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FortCode.Repositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<UserDetailViewModel>> GetUsers();
        Task<UserDetailViewModel> GetUser(int id);
        Task<User> UpdateUser(int id, UserViewModel user);
        Task<User> AddUser(UserViewModel user);
        Task<bool> DeleteUser(int id);
    }
}
