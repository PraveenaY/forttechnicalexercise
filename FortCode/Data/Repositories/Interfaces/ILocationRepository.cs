﻿using FortCode.Models;
using FortCode.Models.DTO;
using FortCode.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Repositories
{
    public interface ILocationRepository
    {
        Task<List<LocationDetailViewModel>> GetLocationDetail(int id);
        Task AddLocationDetail(int userId, LocationViewModel locationDetail);
        Task<bool> FindLocation(int userId, string city, string country);
        Task<bool> DeleteLocation(int userId, int locationId);
    }
}
