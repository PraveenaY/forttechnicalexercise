﻿using FortCode.Data;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using FortCode.Exceptions;
using FortCode.Models.Entities;
using FortCode.Models.DTO;

namespace FortCode.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserDbContext _context;
        public UserRepository(UserDbContext context)
        {
            _context = context;
        }

        public async Task<UserDetailViewModel> GetUser(int id)
        {
            return await _context.Users.Where(u => u.Id == id)
                .Include(i => i.LocationDetail)
                 .Select(user => new UserDetailViewModel
                 {
                     Name = user.Name,
                     Email = user.Email,
                     LocationDetail = user.LocationDetail
                 }).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<UserDetailViewModel>> GetUsers()
        {
            return await _context.Users.Include(i => i.LocationDetail)
                .Select(user => new UserDetailViewModel
                {
                    Name = user.Name,
                    Email = user.Email,
                    LocationDetail = user.LocationDetail
                }).ToListAsync();
        }

        public async Task<User> AddUser(UserViewModel userModel)
        {
            if (userModel.Email == null)
            {
                throw new ArgumentNullException("Email is Required");
            }

            if (userModel.Name == null)
            {
                throw new ArgumentNullException("Name is Required");
            }

            if (userModel.Password == null)
            {
                throw new ArgumentNullException("Password is Required");
            }

            var user = _context.Users.FirstOrDefault(e => e.Email == userModel.Email);
            if (user == null)
            {
                var userData = new User()
                {
                    LocationDetail = null,
                    Name = userModel.Name,
                    Email = userModel.Email,
                    Password = userModel.Password
                };
                _context.Users.Add(userData);
                await _context.SaveChangesAsync();
                return userData;
            }
            else
            {
                throw new AppException("Email already exists.");
            }
        }

        public async Task<User> UpdateUser(int id, UserViewModel user)
        {
            var userData = _context.Users.FirstOrDefault(e => e.Id == id);
            if(userData != null)
            {
                userData.Email = user.Email;
                userData.Name = user.Name;
                userData.Password = user.Password;
                _context.Entry(userData).CurrentValues.SetValues(user);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new AppException("User does not exist.");
            }
            return userData;
        }

        public async Task<bool> DeleteUser(int id)
        {
            var userData = _context.Users.FirstOrDefault(e => e.Id == id);
            if (userData != null)
            {
                _context.Users.Remove(userData);
                var result = await _context.SaveChangesAsync();
                return result > 0;
            }
            return false;
        }
    }
}
