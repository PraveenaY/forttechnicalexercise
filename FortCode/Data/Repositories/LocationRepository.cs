﻿using FortCode.Data;
using FortCode.Models;
using FortCode.Models.DTO;
using FortCode.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Repositories
{
    public class LocationRepository : ILocationRepository
    {
        private readonly UserDbContext _context;
        public LocationRepository(UserDbContext context)
        {
            _context = context;
        }

        public async Task<List<LocationDetailViewModel>> GetLocationDetail(int id)
        {
            var objectList = await _context.Location
               .Where(w => w.UserId == id)
               .Select(location => new LocationDetailViewModel
               {
                   UserId = location.UserId,
                   CityName = location.CityName,
                   Country = location.Country
               }).ToListAsync();
            return objectList;
        }

        public async Task<bool> FindLocation(int userId, string city, string country)
        {
            return await _context.Location
               .Where(w => w.CityName == city && w.Country == country && w.UserId == userId).CountAsync() > 0;
        }

        public async Task AddLocationDetail(int userId, LocationViewModel locationDetail)
        {
            var userData = new Location()
            {
                CityName = locationDetail.CityName,
                Country = locationDetail.Country,
                UserId = userId,
                User = null
            };

            var user = _context.Users.FirstOrDefault(e => e.Id == userData.UserId);
            if (user != null)
            {
                _context.Location.Add(userData);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<bool> DeleteLocation(int userId, int locationId)
        {
            var result = 0;
            var location = await _context.Location
               .Where(w => w.Id == locationId && w.UserId == userId).FirstOrDefaultAsync();
            if (location != null)
            {
                _context.Location.Remove(location);
                result = await _context.SaveChangesAsync();
            }
            return result > 0;
        }
    }
}
