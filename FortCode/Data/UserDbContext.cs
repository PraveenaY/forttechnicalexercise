﻿using FortCode.Models;
using FortCode.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Data
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> options)
            : base(options)
        { }
        // DbSet
        public DbSet<User> Users { get; set; }
        public DbSet<Location> Location { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Location>()
                .HasOne(b => b.User)
                .WithMany(i => i.LocationDetail);
        }

    }
}


